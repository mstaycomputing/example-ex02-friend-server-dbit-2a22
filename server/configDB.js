module.exports = {
    mysql: {
        host: "localhost",
        database: "friendappdb", // must match schema name created in MySQLWorkbench
        username: "friendappuser", // must match username created in MySQLWorkbench
        password: "password123",  // must match user's password set in MySQLWorkbench (NOT the root user)
        logging: console.log
    },
    seed: true // switch to false if you don't want the DB to be refreshed every time you restart server
}