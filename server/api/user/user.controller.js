// extra feature - JWT (not needed for CA2)
// watch the JWT videos yourself to finish it up if you want to fully implement it
const jwt = require('jsonwebtoken');

const SECRET_KEY = "my_secret";

exports.login = (req, resp) => {
    if (!req.body.info) {
        handleErr(resp);
    }
    else {
        info = req.body.info;

        if (!info.username || !info.password) {
            handleErr(resp);
        }
        else {
            if (info.username == "ABC" && info.password == "123") {
                let payload = { subject: 1 };
                let token = jwt.sign(payload, SECRET_KEY);

                resp.status(200).send({token});
            }
            else {
                resp.status(401).send("Unauthorised access");
            }
        }
    }
};




















// Error handling 
function handleErr(res) {
    handleErr(res, null);
}

function handleErr(res, err) {
    console.log(err);
    res.status(500).json({error: true});
}