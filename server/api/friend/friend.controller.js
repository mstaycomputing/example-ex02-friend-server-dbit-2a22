// You should only neeed to require the model you are controlling
var Friend = require('../../database').Friend;

// don't require other models here (unless it is necessary)
// e.g if you want to control User, create another file 'user.controller.js' under another folder 'user'

// this function is triggered from app.js with GET method on /api/friends
exports.list = function (req, resp) {

    // converts to a SELECT * (SQL) command
    Friend.findAll().then(
        (result) => { // result is an ARRAY of Friends (all friends!)
            resp.status(200); // 200 means OK
            resp.type("application/json"); // tells the receiver to prepare to receive JSON type
            resp.json(result); // sends the ARRAY of Friends out
        }
    ).catch(
        (err) => {
            handleErr(resp); // refer to handleErr function below
        }
    );
}

// this function is triggered from app.js with GET method on /api/friend/:fr_id
exports.listOne = function(req, resp) {

    // this means "find by primary key"
    // converts to a SELECT * (SQL) command, with a WHERE condition to match primary key
    Friend.findByPk(
        req.params.fr_id // fr_id is a parameter name, must match with what you wrote in app.js
    ).then(
        (result) => { // result is a single Friend
            resp.status(200); // 200 means OK
            resp.type("application/json"); // tells the receiver to prepare to receive JSON type
            resp.json(result); // sends the Friend out
        }
    ).catch(
        (err) => {
            handleErr(resp); // refer to handleErr function below
        }
    );
}

// this function is triggered from app.js with POST method on /api/friends
exports.add = function(req, resp) {

    // checking if "info" exists in the request body
    if (!req.body.info) {
        handleErr(resp);
    }

    else {
        var newInfo = req.body.info;
        delete newInfo['fr_id']; // fr_id must be removed as the database will allocate the fr_id automatically
        console.log(newInfo);

        // converts into INSERT INTO (SQL) command 
        Friend.create(newInfo).then(
            (newRecord) => { // newRecord is the Friend object that's newly added into the database

                // a shorter way to set status code, type and send the result together
                resp.status(200).type('application/json').json(newRecord);
            }
        ).catch(
            (err) => {
                handleErr(resp); // refer to handleErr function below
            }
        )
    }
}

// this function is triggered from app.js with DELETE method on /api/friend/:fr_id
exports.delete = function(req, resp) {

    // can be modified to fit other conditions
    // this condition matches by primary key (fr_id)
    // so only either 1 row or no rows should be matched to the fr_id and deleted
    var whereClause = {
        fr_id: parseInt(req.params.fr_id) // fr_id is a parameter name, must match with what you wrote in app.js
    };

    // converts to DELETE FROM (SQL) command, with a WHERE condition
    Friend.destroy(
        {where: whereClause} // the condition declared above is transferred over here
    ).then(
        (result) => { // result is the number of rows deleted
            console.log(result);

            if (result == 1) { // if 1 row was deleted, means all is well
                resp.status(200);
                resp.type("application/json");

                // this is customised JSON object for us to tell the receiver what happened
                // you will need to bear this structure in mind when you handle it on Angular side
                resp.json({
                    success: true,
                    message: result + " rows deleted"
                });
            }
            else { // if no rows (or more than one row) deleted, means something's wrong (either fr_id doesn't exist, or there's more than one row with the same fr_id)
                resp.status(200); // we still send 200 (OK) because the DELETE command still executed correctly
                resp.type("application/json");

                // this is customised JSON object for us to tell the receiver what happened
                // you will need to bear this structure in mind when you handle it on Angular side
                resp.json({
                    success: false,
                    message: result + " rows deleted"
                });
            }
        }
    ).catch(
        (err) => {
            handleErr(resp); // refer to handleErr function below
        }
    );
}

// this function is triggered from app.js with PUT method on /api/friend/:fr_id
exports.update = function(req, resp) {

    // checking if "info" exists in the request body
    // for PUT method, there must be a request body (otherwise, how do we know what info to update?)
    if (!req.body.info) {
        handleErr(resp);
    }

    else {
        // grab the request body and load it into a temporary variable
        var updateInfo = req.body.info;

        // can be modified to fit other conditions
        // this condition matches by primary key (fr_id)
        // so only either 1 row or no rows should be matched to the fr_id and deleted
        var whereClause = {
            fr_id: parseInt(req.params.fr_id) // fr_id is a parameter name, must match with what you wrote in app.js
        };

        // converts to UPDATE <table> SET ... (SQL) command with a WHERE condition
        Friend.update(
            updateInfo, // this is the request body (we loaded it into temporary variable updateInfo above)
            {where: whereClause} // the condition declared above is transferred over here
        ).then(
            (result) => { // result is an array, first item is number of rows updated

                console.log(result);

                if (result[0] == 1) { // if 1 row was updated, means all is well
                    resp.status(200);
                    resp.type("application/json");

                    // this is customised JSON object for us to tell the receiver what happened
                    // you will need to bear this structure in mind when you handle it on Angular side
                    resp.json({
                        success: true,
                        message: result[0] + " rows updated"
                    });
                }

                // if no rows or more than 1 row updated, the code will jump here
                // this could mean either:
                // - the fr_id doesn't exist (0 rows updated)
                // - no new info was updated (0 rows updated) (so fr_id exists, but there's no change)
                // - there is more than 1 row with the same fr_id (e.g. 2 rows updated).. shouldn't happen
                else {
                    resp.status(200); // we still send 200 (OK) because the UPDATE command still executed correctly
                    resp.type("application/json");

                    // this is customised JSON object for us to tell the receiver what happened
                    // you will need to bear this structure in mind when you handle it on Angular side
                    resp.json({
                        success: false,
                        message: result[0] + " rows updated"
                    });
                }
            }
        ).catch(
            (err) => {
                handleErr(resp); // refer to handleErr function below
            }
        )
    }
}

// extra function: to search by something other than primary key
// this function is triggered from app.js with GET method on /api/friends/address/:addr
exports.listByAddress = function(req, resp) {

    // modify the condition according to what you want to search by
    var whereClause = {
        // address is the field name you defined in friend.model.js (it is the table column name)
        address: req.params.addr // addr is a parameter name, must match with what you wrote in app.js
    };

    // converts to a SELECT * (SQL) command with a WHERE condition
    Friend.findAll(
        {where: whereClause} // the condition declared above is transferred over here
    ).then(
        (result) => { // result will be an ARRAY of Friends which match the condition
            resp.status(200);
            resp.type("application/json");
            resp.json(result);
        }
    ).catch(
        (err) => {
            handleErr(resp); // refer to handleErr function below
        }
    );
}


// Error handling (this is a generic block of functions to be used in ALL controllers)
// copy and paste these 2 functions into ALL controllers
function handleErr(res) {
    handleErr(res, null);
}

function handleErr(res, err) {
    console.log(err);
    res.status(500).json({error: true});
}



//////////////////// this is dummy code used for the old, non-database implementation ////////////////////
// var friendList = [
//     {fr_id: 1, name: 'Mary Lim'      , email:'mary@yahoo.com'    , contact: '69054637', address: 'Bedok'},
//     {fr_id: 2, name: 'Peter Ho'      , email:'pete@gmail.com'    , contact: '54629874', address: 'Jurong West'},
//     {fr_id: 3, name: 'June Lee'      , email:'jl@yahoo.com.sg'   , contact: '12457898', address: 'Woodlands'},
// ];
//
// exports.list = function (req, resp) {
//     resp.status(200)
//     resp.type("application/json");
//     resp.json(friendList);
// }
//
// exports.add = function(req, resp) {
//     if (!req.body.info) {
//          handleErr(resp);
//      } else {
//         var newinfo = req.body.info;
//
//         friendList.push(newinfo);
//         resp.status(200)
//         resp.type("application/json");
//         resp.json(newinfo);
//     }
// }
//
// exports.delete = function(req, resp) {
//     if (!req.params.fr_id) {
//         handleErr(resp);
//     }
//     else {
//         friendList.splice(req.params.fr_id - 1, 1);
//         resp.status(200).json({"message": "friend deleted"});
//     }
// }
//////////////////// this is dummy code used for the old, non-database implementation ////////////////////