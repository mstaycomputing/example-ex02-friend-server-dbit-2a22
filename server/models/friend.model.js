var Sequelize = require('sequelize');

// refer to CHAPTER 12 for standard template to create a model
// for FOREIGN KEY CONSTRAINTS (needed for categoryID) - refer to CHAPTER 12 (furniture app example)
module.exports = function (database) {
    return database.define('friend', {
        fr_id: {
            type: Sequelize.INTEGER, // refer to: https://sequelize.org/master/manual/model-basics.html#data-types
            primaryKey: true, // only 1 field should be the primaryKey!
            autoIncrement: true, // (usually) only set autoIncrement to true for primaryKey field!
            allowNull: false // depends on whether you would like to allow null / empty values
        },
        name: {
            type: Sequelize.STRING(100),
            primaryKey: false,
            autoIncrement: false,
            allowNull: false
        },
        email: {
            type: Sequelize.STRING(100),
            primaryKey: false,
            autoIncrement: false,
            allowNull: false
        },
        contact: {
            type: Sequelize.STRING(20),
            primaryKey: false,
            autoIncrement: false,
            allowNull: false
        },
        address: {
            type: Sequelize.STRING(100),
            primaryKey: false,
            autoIncrement: false,
            allowNull: false
        }
    }, {
        tableName: 'friends', // remember to name the table properly (it will reflect in your MySQL workbench)
        timestamps: false
    });
}