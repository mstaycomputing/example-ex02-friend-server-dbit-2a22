var configDB = require('./configDB');
var database = require('./database');

// grabs the Sequelize model of a Friend that was exported from the database.js file
var Friend = database.Friend;

// you can declare other models here as needed
// e.g.
// var User = database.User;

module.exports = function () {
    // this only executes if seed is set to true in configDB
    if (configDB.seed) {
        // accesses the friend table (the connection is automatically made when you created the Friend model)
        // and then do a bulk creation of friends
        Friend.bulkCreate([
            {name: 'John Lim'       , email: 'john@yahoo.com'   , contact: '123456' , address: 'Bedok'},
            {name: 'Mary Sue'       , email: 'mary@gmail.com'   , contact: '6544321', address: 'Jurong'},
            {name: 'Peter Lim'      , email: 'peter@hotmail.com', contact: '328762' , address: 'Clementi'},
            {name: 'Tan Chee Seong' , email: 'tancs@hotmail.com', contact: '0982378', address: 'Dover'}
        ]).then(function () {
            console.log("Done creating friend records");
        });

        // you may add additional bulkCreate statements here where needed
        // e.g.
        // User.bulkCreate([
        //     {username: 'admin'  , password:'123'},
        //     {username: 'admin2' , password:'456'}
        // ]).then(function() {
        //     console.log("Done creating user records");
        // })
    }
}