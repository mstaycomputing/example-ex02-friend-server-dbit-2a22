var Sequelize = require('sequelize');
var configDB = require('./configDB');

var database;

// this creates a connection to your MySQL database based on the connection data set in configDB.js
database = new Sequelize(
    configDB.mysql.database,
    configDB.mysql.username,
    configDB.mysql.password,
    {
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        },
        logging: configDB.mysql.logging
    }
);

// this is a Sequelize model of a Friend as defined in friend.model.js
var Friend = require('./models/friend.model')(database);

// you may add additional models here where needed
// e.g.
// var User = require('./models/user.model')(database);

// this step gets your MySQL database to sync up with your seed data defined in seed.js
// it will only execute (force) when seed is set to true in configDB.js
database.sync({
    force: configDB.seed
}).then(function () {
    require('./seed')();
    console.log("Database in sync now.");
});

// this exposes the Sequelize Friend model (declared above) for all other files in the server to use
module.exports = {
    Friend: Friend
    // you may add additional models here where needed (add a comma in the line above)
    // e.g.
    // User: User
}