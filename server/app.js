var express     = require ('express');
var bodyParser  = require('body-parser');
var friendCtrl  = require('./api/friend/friend.controller');
var userCtrl    = require('./api/user/user.controller');
// import additional controllers here as needed

//// start of standard setup code for your express app
var app  = express();
var cors = require('cors');
var path = require('path');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); 
app.use(cors());
app.use(express.static(path.join(__dirname, "../public"))); // this line causes our root directory http://localhost:3000 to access "public" folder
//// end of standard setup code for your express app

// define your API endpoints here (take note of plural / singular!!!)
app.get     ("/api/friends"            , friendCtrl.list);
app.get     ("/api/friend/:fr_id(\\d+)", friendCtrl.listOne); // fr_id is a parameter name, must match with what you write in friend.controller.js
app.post    ("/api/friends"            , friendCtrl.add);
app.delete  ("/api/friend/:fr_id(\\d+)", friendCtrl.delete); // fr_id is a parameter name, must match with what you write in friend.controller.js
app.put     ("/api/friend/:fr_id(\\d+)", friendCtrl.update); // fr_id is a parameter name, must match with what you write in friend.controller.js

// extra example you may find useful: how to get a friend by something other than primary key (e.g. address)
app.get     ("/api/friends/address/:addr", friendCtrl.listByAddress); // addr is a parameter name, must match with what you write in friend.controller.js

// extra feature for JWT (not needed for CA2)
app.post("/api/login", userCtrl.login);

// standard response if a user navigates to the root (e.g. http://localhost:3000)
app.use(function (req, resp) {
    resp.status(404);
    resp.send("Error File not Found!!!!!!!!!!");
});

// set port and start webserver
// stick to port 3000 unless there's a conflict with other existing apps
app.listen('3000', function () {
    console.log("Server running at http://localhost:3000");
});
